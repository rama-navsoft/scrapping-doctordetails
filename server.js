
const cheerio=require("cheerio");
const express = require('express');
const fs = require('fs');
const request = require('request');
var htmlToJson = require('html-to-json');
const jsdom = require("jsdom");
const { JSDOM } = jsdom;
var async = require("async");
  

let doctors=[];

fs.readFile(__dirname + '/html/doctors.html', 'utf8', function(err, html){
    //console.log(html);
    var htmlVals,jsHtml,x;
    htmlVals=html;
    const dom = new JSDOM(html);
    const $ = (require('jquery'))(dom.window);
    $("#FacilitySearch_Doctor option").each(function()
    {
    
    doctors.push({id:$(this).val()}); 
   
    fs.appendFile(__dirname+"/doctorid.json", JSON.stringify(
        {id:$(this).val()}, null, 4),function (err) {
        
    if(err){
                console.log("Error While Appending the Data !");
            }
            //console.log("Data Appended Successfully !");
        })});
    //console.log(doctors);
   
    console.log(doctors.length)
 for (let i=0;i<doctors.length;i++){
    let doctorjson = {
        doctor_img:"",
        doctor_name : "",
        doctor_profession_summary:"",
        doctor_qualification_details:"",
        doctor_affiliation_details:"",
        doctor_insuranceaffilation_details:"",
        doctor_language_details:"",
        doctor_personal_details:"",
        doctor_practice:""
    }   

   
//request("https://daktariafrica.com/doctor/817/",(error,response,html)
    request("https://daktariafrica.com/doctor/"+doctors[i].id,(error,response,html)=>{
    
    if(!error && response.statusCode==200){
    const $=cheerio.load(html);
    
    //doctor details name with biodata
    $('.right').filter(function () {
    var data = $(this); 
    doctorbiodata=data.text();
    })
    
    
    //console.log(doctorbiodata)
    console.log("------------------------------------------------------------")
    console.log("doctor name onloy");
    $('.user-pic').filter(function () {
        let img = $(this); 
        var imgSrc=img.find('img').attr('src');
        doctorjson.doctor_img = 'https://daktariafrica.com'+imgSrc;
        // console.log('imgSrc');
        // console.log(imgSrc);
        // console.log('end imgSrc');
    })


    console.log("------------------------------------------------------------")
    console.log("doctor name onloy");

    //doctor name 
    const doctorname=$('.name');
    console.log(doctorname.text());
    doctorjson.doctor_name = doctorname.text().replace("\n"," ").replace("\t"," ").trim();
    
    
    console.log("------------------------------------------------------------")
    console.log("professional summary medicale board number");
    
    //professional summary biodata details
    $('.specs').filter(function () {
        let details = $(this); 
        professionalsummarydata=details.text().replace("\n\t\t\t\t\t"," ")
        .replace("\n\t\t\t\n\t\t\t\t\n\t\t\t\n\t\t\t\n\t\t\t"," ").replace("\t\t\t\t\t\t\t","").
        replace("Profession Summary"," ").trim();
        })
        console.log(professionalsummarydata)
    doctorjson.doctor_profession_summary = professionalsummarydata
   
   
    //doctor qualification details    
    
    console.log("------------------------------------------------------------")
        console.log("doctor qualifications")
        $('.qualifications').filter(function () {
        let details = $(this); 
        doctorqualification=details.text().replace("\n"," ").replace("\t"," ").
        replace("\t\t\tAdmission Rights\n\t\t\t\t\t\t\t","Admission Rights").
        replace("\t\t\tAdmission Rights\n\t\t\t\t\t\t\n\t\t","Admission Rights").
        replace("Professional Training\n\t\t\t\t\t\t\t\t\t\n\t\t\t\n\t\t\t\t","Professional Training:").
        replace("Professional Training\n\t\t\t\t\t\t\t","Professional Training:")
        .trim();
        console.log(doctorqualification);    
        //details individual
        
        console.log((details).children('h4').text());
        console.log(details.children('div').find('h4').text())
        doctorjson.doctor_qualification_details = doctorqualification;
    })
    
    
    
    //doctor hospital affiliation details  
    console.log("------------------------------------------------------------")
    console.log("doctor hostpital affiliation details");
    $('.hospital-affiliation').filter(function () {
    let details = $(this); 
    doctorhospitalaffiliation=details.text().replace("\n"," "),("\t"," ")
    .replace(" \t\t\tAdmission Rights\n\t\t\t\t\t\t\n\t\t","Admission Rights:")
    .trim();
    console.log(doctorhospitalaffiliation);    
    //hospital affiliation details individuals
    console.log((details).children('h4').text());
    
    doctorjson.doctor_affiliation_details = doctorhospitalaffiliation;


})
        
    
    
    //doctor insurance affiliation details  
    console.log("------------------------------------------------------------")
    console.log("doctor insurance affiliation details");
  
   
    $('.insurance-affiliations').filter(function () {
        let details = $(this); 
        doctorinsuranceaffiliation=details.text().replace("\n"," ").replace("\t"," ").trim();
        console.log(doctorinsuranceaffiliation);    
        
     //insurance affiliation details individuals
     console.log((details).children('h4').text())
    
    doctorjson.doctor_insuranceaffilation_details=doctorinsuranceaffiliation;
    
    })
      
        
    

    //doctor language spoken details  
        
    console.log("------------------------------------------------------------")
    console.log("doctor language spoken details");


        $('.language').filter(function () {
        let details = $(this); 
        doctorlanguage=details.text().replace("\n"," ").replace("\t"," ")
        .replace("\n\t\t\t\t\t\t\t"," ").replace("\t\t\t\t\t\t\tSwahili","Swahili").
        replace("\t\t\t\t\t\t\tEnglish","English").replace("\t\t\t\t\t\t\tKimeru","Kimeru")
        .replace("Languages Spoken","Languages Spoken:").trim()
        
        
        console.log(doctorlanguage);    
    //language spoken details

    console.log((details).children('h4').text());

        doctorjson.doctor_language_details=doctorlanguage;


    })
  



    //doctor personal details  
    console.log("------------------------------------------------------------")
    console.log("doctor personal details");

    $('.peronal-details').filter(function () {
        let details = $(this); 
        doctorpersonaldetails=details.text().replace("\n"," ").replace("\t"," ")
        .replace("Gender:\n\t\t\t","Gender:").replace("\n\n\t\t\tHobbies\t:","Hobbies:").trim();
        console.log(doctorpersonaldetails);    
    doctorjson.doctor_personal_details=doctorpersonaldetails;    
    
    
    })


    //doctor practice biodata details  
    console.log("------------------------------------------------------------")
    console.log("doctor practice details");

    $('.practice').filter(function () {
        let details = $(this); 
        doctorpractice=details.text()
        .replace("\n\t\t  \t\t\t\n\t\t\tPractice\n\t\t\tEmail:\n\t\t\t"," ").
        replace("\n\t\t\tPhone:").replace("undefined"," ").trim();
        doctorjson.doctor_practice=doctorpractice;
        console.log(doctorpractice);    
        })


fs.appendFile(__dirname+"/doctordetails1.json",JSON.stringify(doctorjson,null,4),function (err) {
            if(err){
                console.log("Error While Appending the Data !");
            }
            console.log("Data Appended Successfully !");
})
    
    console.log("------------------------------------------------------------")

}})}})





